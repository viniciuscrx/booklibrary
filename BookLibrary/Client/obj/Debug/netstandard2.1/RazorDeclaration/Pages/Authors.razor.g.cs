#pragma checksum "C:\Users\vinic\source\repos\BookLibrary\Client\Pages\Authors.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7e155b3ce2ab0027b6ace9a791d744f1e0f11348"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace BookLibrary.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\vinic\source\repos\BookLibrary\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\vinic\source\repos\BookLibrary\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\vinic\source\repos\BookLibrary\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\vinic\source\repos\BookLibrary\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\vinic\source\repos\BookLibrary\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\vinic\source\repos\BookLibrary\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\vinic\source\repos\BookLibrary\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\vinic\source\repos\BookLibrary\Client\_Imports.razor"
using BookLibrary.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\vinic\source\repos\BookLibrary\Client\_Imports.razor"
using BookLibrary.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\vinic\source\repos\BookLibrary\Client\Pages\Authors.razor"
using BookLibrary.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\vinic\source\repos\BookLibrary\Client\Pages\Authors.razor"
using BookLibrary.Shared.Entities;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/author")]
    public partial class Authors : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 48 "C:\Users\vinic\source\repos\BookLibrary\Client\Pages\Authors.razor"
       
    private Author[] _authors;

    private bool ShowMessage = false;

    protected override async Task OnInitializedAsync()
    {
        _authors = await Http.GetFromJsonAsync<Author[]>("Author");
    }

    private async Task delAuthor(int id)
    {
        if (await validateDelete(id))
        {
            await Http.DeleteAsync("Author/" + id.ToString());
            _authors = await Http.GetFromJsonAsync<Author[]>("Author");
        }
        else
        {
            ShowMessage = true;

        }
    }

    private void addAuthor()
    {
        NavigationManager.NavigateTo("/authoradd");
    }

    private async Task<bool> validateDelete(int authorId)
    {
        var books = await Http.GetFromJsonAsync<Book[]>("/Book");
        return !books.Where(x => x.AuthorId == authorId).Any();
    }

    private void editAuthor(int authorId)
    {
        NavigationManager.NavigateTo("/authoredit/" + authorId.ToString());
    }

    private void HideModal()
    {
        ShowMessage = false;
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
