﻿using BookLibrary.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Repository.Interfaces
{
    public interface IAuthorRepository : IRepository<Author>
    {
        public Task EditAndSaveAsync(Author entity);
    }
}
