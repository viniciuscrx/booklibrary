﻿using BookLibrary.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Repository.Interfaces
{
    public interface IPublisherRepository : IRepository<Publisher>
    {
        public Task EditAndSaveAsync(Publisher entity);
    }
}
