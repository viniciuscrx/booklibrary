﻿using BookLibrary.Repository.Interfaces;
using BookLibrary.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Repository.Repositories
{
    public class AuthorRepository : BaseRepository<Author>, IAuthorRepository
    {
        public AuthorRepository(DbContext context) : base(context) { }

        public async Task EditAndSaveAsync(Author entity)
        {
            var editedEntity = await DbSet.FirstOrDefaultAsync(e => e.Id == entity.Id);
            editedEntity.Id = entity.Id;
            editedEntity.Name = entity.Name;
            _context.SaveChanges();
        }

    }
}
