﻿using BookLibrary.Repository.Interfaces;
using BookLibrary.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Repository.Repositories
{
    public class BookRepository : BaseRepository<Book>, IBookRepository
    {
        public BookRepository(DbContext context) : base(context) { }

        public new IEnumerable<Book> GetAll()
        {
            return DbSet.Include(book => book.Author)
                .Include(book => book.Publisher)
                .Select(x => new Book() { Title = x.Title, 
                    Id = x.Id, 
                    CreationDate = x.CreationDate, 
                    AuthorId = x.AuthorId, 
                    PublisherId = x.PublisherId, 
                    Author = new Author() { Name = x.Author.Name, Id = x.Author.Id},
                    Publisher = new Publisher() { Name = x.Publisher.Name, Id = x.Publisher.Id}
                })
                .ToList();
        }


        public Book GetById(int id)
        {
            return DbSet.Include(book => book.Author).Include(book => book.Publisher).ToList().Find(x => x.Id == id);
        }

        public async Task EditAndSaveAsync(Book entity)
        {
            var editedEntity = await DbSet.FirstOrDefaultAsync(e => e.Id == entity.Id);
            editedEntity.Id = entity.Id;
            editedEntity.Title = entity.Title;
            editedEntity.AuthorId = entity.AuthorId;
            editedEntity.PublisherId = entity.PublisherId;
            editedEntity.CreationDate = entity.CreationDate;
            _context.SaveChanges();
        }

    }
}
