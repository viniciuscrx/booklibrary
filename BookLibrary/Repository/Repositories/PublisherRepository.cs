﻿using BookLibrary.Repository.Interfaces;
using BookLibrary.Shared.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookLibrary.Repository.Repositories
{
    public class PublisherRepository : BaseRepository<Publisher>, IPublisherRepository
    {
        private new DbContext _context;
        private new DbSet<Publisher> DbSet;
        public PublisherRepository(DbContext context) : base(context)
        {
            _context = context;
            DbSet = _context.Set<Publisher>();
        }

        public async Task EditAndSaveAsync(Publisher entity)
        {
            var editedEntity = await DbSet.FirstOrDefaultAsync(e => e.Id == entity.Id);
            editedEntity.Id = entity.Id;
            editedEntity.Name = entity.Name;
            _context.SaveChanges();
        }

    }
}
