﻿using BookLibrary.Repository.Interfaces;
using BookLibrary.Shared.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BookLibrary.Repository.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {

        protected readonly DbContext _context;
        protected DbSet<TEntity> DbSet;

        public BaseRepository(DbContext context)
        {
            _context = context;
            DbSet = _context.Set<TEntity>();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return DbSet.ToList();
        }


        public TEntity GetById<T>(int id)
        {
            return DbSet.Find(id);
        }

        public void Create(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void Edit(TEntity entity)
        {
            var editedEntity = DbSet.FirstOrDefault(e => e.Id == entity.Id);
            editedEntity = entity;
        }

        public virtual void Delete<T>(int id)
        {
            DbSet.Remove(GetById<T>(id));
        }

        public void SaveChanges() => _context.SaveChanges();

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }


    }
}
