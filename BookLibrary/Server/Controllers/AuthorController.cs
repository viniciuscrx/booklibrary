﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookLibrary.Service.Interfaces;
using BookLibrary.Shared.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BookLibrary.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        // GET: api/<AuthorController>
        [HttpGet]
        public IEnumerable<Author> Get()
        {
            return _authorService.GetAll();
        }

        // GET api/<AuthorController>/5
        [HttpGet("{id}")]
        public Author Get(int id)
        {
            return _authorService.GetById<Author>(id);
        }

        // POST api/<AuthorController>
        [HttpPost]
        public void Post([FromBody] Author aut)
        {
            _authorService.Create(aut);
        }

        // PUT api/<AuthorController>
        [HttpPut]
        public void Put([FromBody] Author aut)
        {
            _authorService.Edit(aut);
        }

        // DELETE api/<AuthorController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _authorService.Delete<Author>(id);
        }
    }
}
