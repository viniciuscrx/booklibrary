﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookLibrary.Service.Interfaces;
using BookLibrary.Shared.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BookLibrary.Server.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PublisherController : ControllerBase
    {
        private readonly IPublisherService _publisherService;
        
        public PublisherController(IPublisherService publisherService)
        {
            _publisherService = publisherService;
        }
        // GET: api/<PublisherController>
        [HttpGet]
        public IEnumerable<Publisher> Get()
        {
            return _publisherService.GetAll();
        }

        // GET api/<PublisherController>/5
        [HttpGet("{id}")]
        public Publisher Get(int id)
        {
            return _publisherService.GetById<Publisher>(id);
        }

        // POST api/<PublisherController>
        [HttpPost]
        public void Post([FromBody] Publisher pub)
        {
            _publisherService.Create(pub);
        }

        // PUT api/<PublisherController>
        [HttpPut]
        public void Put([FromBody] Publisher pub)
        {
            _publisherService.Edit(pub);
        }

        // DELETE api/<PublisherController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _publisherService.Delete<Publisher>(id);
        }
    }
}
