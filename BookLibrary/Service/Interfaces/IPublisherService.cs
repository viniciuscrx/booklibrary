﻿using BookLibrary.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary.Service.Interfaces
{
    public interface IPublisherService : IService<Publisher>
    {
    }
}
