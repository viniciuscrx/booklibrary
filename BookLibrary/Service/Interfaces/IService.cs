﻿using BookLibrary.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary.Service.Interfaces
{
    public interface IService<TEntity> where TEntity : IEntity
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById<T>(int id);
        void Create(TEntity entity);
        void Edit(TEntity entity);
        void Delete<T>(int id);
        void SaveChanges();
        void Dispose();
    }
}
