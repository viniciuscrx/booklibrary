﻿using BookLibrary.Repository.Interfaces;
using BookLibrary.Service.Interfaces;
using BookLibrary.Shared.Entities;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace BookLibrary.Service.Services
{
    public class AuthorService : BaseService<Author>, IAuthorService
    {
        protected readonly IAuthorRepository _authorRepository;
        protected readonly IBookRepository _bookRepository;

        public AuthorService(IAuthorRepository authorRepository, IBookRepository bookRepository) : base(authorRepository)
        {
            _authorRepository = authorRepository;
            _bookRepository = bookRepository;
        }

        public new void Create(Author entity)
        {
            _authorRepository.Create(entity);
            _authorRepository.SaveChanges();
        }

        public new void Edit(Author entity)
        {
            _authorRepository.EditAndSaveAsync(entity).Wait();
        }

        public new void Delete<Author>(int id)
        {
            var books = _bookRepository.GetAll();
            
            if(books.Where(x => x.AuthorId == id).Any())
            {
                throw new Exception("It's not possible to delete an author with books associated");
            }
            _authorRepository.Delete<Author>(id);
            _authorRepository.SaveChanges();
        }
    }
}
