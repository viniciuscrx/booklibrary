﻿using BookLibrary.Repository.Interfaces;
using BookLibrary.Service.Interfaces;
using BookLibrary.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary.Service.Services
{
    public class BaseService<TEntity> : IService<TEntity> where TEntity : class, IEntity
    {
        protected readonly IRepository<TEntity> _repository;

        public BaseService(IRepository<TEntity> repository)
        {
            this._repository = repository;
        }

        public void Create(TEntity entity)
        {
            _repository.Create(entity);
        }

        public void Delete<T>(int id)
        {
            _repository.Delete<T>(id);
        }

        public void Dispose()
        {
            _repository.Dispose();
        }

        public void Edit(TEntity entity)
        {
            _repository.Edit(entity);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _repository.GetAll();
        }

        public TEntity GetById<T>(int id)
        {
           return _repository.GetById<T>(id);
        }

        public void SaveChanges()
        {
            _repository.SaveChanges();
        }
    }
}
