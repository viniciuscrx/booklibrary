﻿using BookLibrary.Repository.Interfaces;
using BookLibrary.Service.Interfaces;
using BookLibrary.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary.Service.Services
{
    public class BookService : BaseService<Book>, IBookService
    {
        protected readonly IBookRepository _bookRepository;

        public BookService(IBookRepository bookRepository) : base(bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public new void Create(Book entity)
        {
            entity.CreationDate = DateTime.Now;
            _bookRepository.Create(entity);
            _bookRepository.SaveChanges();
        }

        public new void Edit(Book entity)
        {
            _bookRepository.EditAndSaveAsync(entity).Wait();
        }

        public new void Delete<Book>(int id)
        {
            _bookRepository.Delete<Book>(id);
            _bookRepository.SaveChanges();
        }
    }
}
