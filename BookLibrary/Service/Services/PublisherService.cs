﻿using BookLibrary.Repository.Interfaces;
using BookLibrary.Service.Interfaces;
using BookLibrary.Shared.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary.Service.Services
{
    public class PublisherService : BaseService<Publisher>, IPublisherService
    {
        protected readonly IPublisherRepository _publisherRepository;

        public PublisherService(IPublisherRepository publisherRepository) : base(publisherRepository)
        {
            _publisherRepository = publisherRepository;
        }

        public new void Create(Publisher entity)
        {
            _publisherRepository.Create(entity);
            _publisherRepository.SaveChanges();
        }

        public new void Edit(Publisher entity)
        {
            _publisherRepository.EditAndSaveAsync(entity).Wait();
        }

        public new void Delete<Publisher>(int id)
        {
            _publisherRepository.Delete<Publisher>(id);
            _publisherRepository.SaveChanges();
        }
    }
}
