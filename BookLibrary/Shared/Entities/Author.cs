﻿using BookLibrary.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace BookLibrary.Shared.Entities
{
    public partial class Author : IEntity
    {
        public Author()
        {
            Book = new HashSet<Book>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Identifier too long (100 character limit).")]
        public string Name { get; set; }

        public virtual ICollection<Book> Book { get; set; }
    }
}
