﻿using BookLibrary.Shared.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;


namespace BookLibrary.Shared.Entities
{
    public partial class Book : IEntity
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Identifier too long (100 character limit).")]
        public string Title { get; set; }
        public int PublisherId { get; set; }
        public int AuthorId { get; set; }

        public virtual Author Author { get; set; }
        public virtual Publisher Publisher { get; set; }
    }
}
