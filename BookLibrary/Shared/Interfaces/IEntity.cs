﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookLibrary.Shared.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
