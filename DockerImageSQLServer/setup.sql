CREATE DATABASE BookLibraryDB
GO
USE BookLibraryDB
GO

CREATE TABLE Publisher(
    Id INT IDENTITY (1,1) PRIMARY KEY NOT NULL,
    Name VARCHAR(100) NOT NULL
)

CREATE TABLE Author(
    Id INT IDENTITY (1,1) PRIMARY KEY NOT NULL,
    Name VARCHAR(100) NOT NULL
)

CREATE TABLE Book (
    Id INT IDENTITY (1,1) PRIMARY KEY NOT NULL,
    CreationDate DATETIME NOT NULL,
    Title VARCHAR(100) NOT NULL,
    PublisherId INT NOT NULL,
    AuthorId INT NOT NULL,
    CONSTRAINT FK_BOOK_PUBLISHER FOREIGN KEY(PublisherId) REFERENCES Publisher(Id),
    CONSTRAINT FK_BOOK_AUTHOR FOREIGN KEY(AuthorId) REFERENCES Author(Id)
    )

INSERT INTO [dbo].[Author] values ('Gabriel Garcia Marquez')
INSERT INTO [dbo].[Author] values ('Harlan Coben')
INSERT INTO [dbo].[Author] values ('Clarice Lispector')
INSERT INTO [dbo].[Author] values ('Paulo Coelho')

INSERT INTO [dbo].[Publisher] values ('Moderna')
INSERT INTO [dbo].[Publisher] values ('Arqueiro')
