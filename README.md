# BookLibrary

Welcome to my small Book Library using C# and Blazor. It was an attempt far from optimal to implement the layer pattern sharing the entities across the whole solution,
it facilitates the pass of data in between the layers, mainly between the controller and the client. But even such small project could use some better separation that would mean
more work during the scafolding of project but it would be much easier later on when implementing new features in the frontend, there were some ideas that would go great if I had
separated viewmodels from the entities. It is not a final version, I might be updating the project and fixing the design flaws and technical faults.

I used docker-compose to help communicate the server container with the database container. 

So we have the booklibrary folder for the database image, it is mainly a default sql server container where I copy some SQL Script with some "default data". The command is simple
enough and to make it compatible with the compose.yml, I recommend naming the tag: booklibrary:latest


Then we have the booklibraryserver folder that is the C# solution folder, the dockerfile is inside the Server folder and the build command should be something like this:
    - docker build -f "path/to/dockerfile" --force-rm -t booklibraryserver:latest "path/to/solution/root"
    - As I mentioned before, to be able to run the compose.yml without changes, name the tag as booklibraryserver:latest
    
The last folder is the composer folder with the yml that will orchestrate the containers, it maps the host port 8000 to the container 80 port, so after starting the compose you can
access webapp through https://localhost:8000

It is a PWA! So you can installed it and have it offline, I didnt do any special treatment for the serviceworker, I was just trying this option for blazor, so please dont expect
much for it without internet.